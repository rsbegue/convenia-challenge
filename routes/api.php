<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::pattern('table_id', '[0-9]+');

Route::apiResource('items',  'ItemsController',  ['only'=>'index']);
Route::apiResource('tables', 'TablesController', ['only'=>'index']);
Route::get('tables/{table_id}/consummation',  'ConsummationsController@getConsummationByTable');
Route::post('tables/{table_id}/payment',      'PaymentsController@store');