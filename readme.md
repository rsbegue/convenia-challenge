# Objetivo

A divisão de contas em restaurantes costuma ser trabalhosa para o garçom, e tem o potencial de gerar confusões. O seu objetivo é criar uma api para facilitar a vida desse garçom.

# Requisitos
Temos um aplicativo mobile que precisa consumir de uma API algumas informações para executar as tarefas abaixo:  

- O garçom pode selecionar a mesa para gerar a conta de cobrança
- Ao selecionar a mesa, é exibida uma lista dos produtos consumidos, os respectivos preços e o valor total a ser pago
- O garçom pode registrar os pagamentos individuais referentes à mesa, caso alguém queira sair antes  
- Devem ser exibidos os pagamentos parciais e o quanto falta para completar o valor total da mesa  

## Importante
- Clareza e organização do código
- É recomendado o uso do framework Laravel
- Utilização dos princípios SOLID  
- As mesas e produtos não precisam ser de fato registrados, podem ser dados mockados se preferir
- Utilizar Docker é um diferencial  
- Não é necessária implementação de front-end  
- Realizar testes é um bom diferencial, mas não obrigatório (Behat melhor ainda =) )  
Divirta-se, seja criativo e sinta-se à vontade para tirar dúvidas.

# Colocando para rodar
### Requisitos
- Docker
- Docker-compose
- Postman

Após baixar o arquivo no repositório basta rodar o arquivo run.sh. Ele é o responsável por criar as instancias no Docker, realizar o migrate/seed no MySQL e servir a API.

    $ git clone https://rsbegue@bitbucket.org/rsbegue/convenia-challenge.git
    $ cd convenia-challenge
    $ bash run.sh
 
 Acesse http://localhost
 
### Feedback
A ideia do desafio foi excelente, pois ele me fez aprofundar mais em alguns conceitos, testes unitário e automatizados e alguns padrões.

Bom, o que acharam?