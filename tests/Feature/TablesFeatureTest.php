<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Tables;
use App\Consummations;

class TablesFeatureTest extends TestCase
{
    public function test_get_tables()
    {
        $response = $this->json('GET', '/api/tables');

        $response->assertStatus(200)->assertJsonStructure([
            'meta',
            'data',
            'links'
        ]);
    }

    public function test_get_tables_not_items()
    {
        $response = $this->json('GET', '/api/tables/'. Tables::where('opened',0)->inRandomOrder()->first()->id .'/consummation');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'items_count' => 0
            ]);
    }

    public function test_get_tables_with_items()
    {
        $tables = Tables::where('opened',1)->inRandomOrder()->first();
        $consummations = Consummations::where('table_id',$tables->id)->count();

        $response = $this->json('GET', '/api/tables/'. $tables->id .'/consummation');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'items_count' => $consummations
            ]);
    }

    public function test_post_payment()
    {
        $tables = Tables::where('opened',1)->inRandomOrder()->first();
        
        $response = $this->json('POST', '/api/tables/'. $tables->id .'/payment', ['paid' => 1]);

        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                'paid' => 1
            ]);
    }
}