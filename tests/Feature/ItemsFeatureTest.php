<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemsFeatureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_items()
    {
        $response = $this->json('GET', '/api/items');

        $response->assertStatus(200)->assertJsonStructure([
            'meta',
            'data',
            'links'
        ]);
    }
}
