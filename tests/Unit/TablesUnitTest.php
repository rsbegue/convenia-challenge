<?php

namespace Tests\Unit;

use App\Tables;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TablesUnitTest extends TestCase
{
    public function test_seed_tables()
    {
        $this->assertEquals(10, Tables::count());
    }
}
