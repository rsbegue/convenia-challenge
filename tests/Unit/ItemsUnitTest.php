<?php

namespace Tests\Unit;

use App\Items;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemsUnitTest extends TestCase
{
    public function test_seed_items()
    {
        $this->assertEquals(12, Items::count());
    }
}
