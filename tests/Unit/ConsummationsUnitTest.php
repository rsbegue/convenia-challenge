<?php

namespace Tests\Unit;

use App\Consummations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConsummationsUnitTest extends TestCase
{
    public function test_seed_consummations()
    {
        $this->assertEquals(20, Consummations::count());
    }
}
