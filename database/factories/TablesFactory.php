<?php

use Faker\Generator as Faker;

$factory->define(App\Tables::class, function (Faker $faker) {

    $actors = array('Iron Man','Spider Man','Hulk','Black Panther','Captain America','Loki','Thor','Black Widow','Hawkeye','Doctor Strange','Ant-Man','War Machine','Falcon','Vision','Thanos');
    
    return [
        'label' => 'Mesa - ' . $faker->unique()->randomElement($actors),
        'opened'=> $faker->boolean(60)
    ];
});
