<?php

use Faker\Generator as Faker;

$factory->define(App\Items::class, function (Faker $faker) {

    $food = array('Burger','X-Burger','X-Egg Bacon','Fries','Chips','Onion Rings','X-Salad','X-Vegan','Juice','Water','Drink','Milkshake','Coffee','Soda');
    
    return [
        'label'   => $faker->unique()->randomElement($food),
        'total'   => $faker->randomFloat(2, $min = 4.99, $max = 29.99),
        'stocked' => $faker->boolean(75)
    ];
});
