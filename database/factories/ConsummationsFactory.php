<?php

use Faker\Generator as Faker;

$factory->define(App\Consummations::class, function (Faker $faker) {
    $item = App\Items::where('stocked',1)->inRandomOrder()->first();
    $quantity   = $faker->randomDigitNotNull();

    return [
        'table_id' => function(){
            return App\Tables::where('opened',1)->inRandomOrder()->first()->id;
        },
        'item_id'     => $item->id,
        'unit_price'  => $item->total,
        'total_price' => $item->total * $quantity,
        'quantity'    => $quantity
    ];
});
