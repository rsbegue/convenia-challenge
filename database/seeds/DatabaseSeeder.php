<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TablesSeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(ConsummationsSeeder::class);
        
        //$this->call(TablesSeeder::class);
        //$this->call(ItemsSeeder::class);
        //$this->call(ConsummationsSeeder::class);
    }
}
