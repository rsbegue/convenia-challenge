<?php

use Illuminate\Database\Seeder;

class ConsummationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $consummations = factory(App\Consummations::class, 20)->create();
    }
}
