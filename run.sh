#!/bin/bash

echo Uploading Application container 
docker-compose up -d

echo Install dependencies
docker exec -it convenia-app composer install

echo Generate key
docker exec -it convenia-app php artisan key:generate

echo Make migrations
docker exec -it convenia-app php artisan migrate

echo Make seeds
docker exec -it convenia-app php artisan db:seed

echo Make Tests
docker exec -it convenia-app vendor/bin/phpunit

echo Swagger documentation
docker exec -it convenia-app php artisan l5-swagger:generate
cp swagger.json ./storage/api-docs/api-docs.json

echo Information of new containers
docker ps