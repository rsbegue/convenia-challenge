<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TablesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id'     => $this->id,
            'label'  => $this->label,
            'opened' => $this->opened,
            'detail' => $this->when($this->consummations->count(), $request->root() .'/api/tables/'.$this->id.'/consummation')
        ];
    }
}
