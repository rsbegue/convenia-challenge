<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TableConsummationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
            'id'             => $this->id,
            'table'          => $this->label,
            'total'          => round($this->consummations->sum('total_price'),2) - round($this->payments->sum('paid')),
            'total_paid'     => round($this->payments->sum('paid')),
            'payments'       => $this->payments,
            'items_count'    => $this->consummations->count(),
            'items'          => ConsummationsResource::collection($this->consummations)
        ];
    }
}
