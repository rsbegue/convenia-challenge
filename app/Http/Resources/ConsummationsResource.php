<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConsummationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'label'         => $this->items->label,
            'unit_price'    => $this->items->total,
            'total_price'   => $this->items->total * $this->quantity,
            'quantity'      => $this->quantity
        ];
    }
}
