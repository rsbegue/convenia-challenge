<?php

namespace App\Http\Controllers;

use App\Payments;
use App\Http\Resources\PaymentsResource;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $table_id)
    {
        $payment = Payments::create(
            [
                'table_id'  => $table_id,
                'paid'      => $request->input('paid')
            ]
        );

        return new PaymentsResource($payment);
    }
}
