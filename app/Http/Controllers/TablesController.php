<?php

namespace App\Http\Controllers;

use App\Tables;
use App\Http\Resources\TablesResource;

use Illuminate\Http\Request;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TablesResource::collection(Tables::with('consummations')->paginate(100));
    }
}
