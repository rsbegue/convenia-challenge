<?php

namespace App\Http\Controllers;

use App\Items;
use App\Http\Resources\ItemsResource;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemsResource::collection(Items::paginate(100));
    }
}
