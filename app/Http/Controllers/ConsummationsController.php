<?php

namespace App\Http\Controllers;

use App\Consummations;
use App\Tables;
use App\Http\Resources\TableConsummationResource;
use Illuminate\Http\Request;

class ConsummationsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Consummations  $consummations
     * @return \Illuminate\Http\Response
     */
    public function getConsummationByTable(Request $request, $table_id)
    {
        return TableConsummationResource::collection(
            Tables::with('consummations')->with('payments')->where('id',$table_id)->get()
        );
    }
}
