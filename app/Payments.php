<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDelete;

class Payments extends Model
{
    protected $fillable = ['table_id', 'paid'];

    public function tables(){
        return $this->belongsTo(Tables::class,'table_id');
    }
}
