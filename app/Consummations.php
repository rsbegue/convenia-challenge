<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDelete;

class Consummations extends Model
{
    protected $fillable = ['table_id', 'item_id', 'unit_price', 'total_price', 'quantity'];

    public function tables(){
        return $this->belongsTo(Tables::class,'table_id');
    }

    public function items(){
        return $this->belongsTo(Items::class,'item_id')->orderBy('label','asc');
    }
}
