<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDelete;

class Tables extends Model
{
    public function consummations(){
        return $this->hasMany(Consummations::class,'table_id');
    }

    public function payments(){
        return $this->hasMany(Payments::class,'table_id');
    }
}
