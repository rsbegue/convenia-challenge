<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDelete;

class Items extends Model
{
    public function consummations(){
        return $this->hasMany(App\Consummations::class);
    }
}
